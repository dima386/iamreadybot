from os import error
from django.core.management.base import BaseCommand
from django.conf import settings
from telegram import Bot, Update
from telegram.ext import CallbackContext, Filters, MessageHandler, Updater
from telegram.utils.request import Request


def log_errors(f):
    def inner(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except Exception as e:
            error_message = f'Произошла ошибка: {e}'
            print(error_message)
            raise e
    return inner

@log_errors
def do_echo(update: Update, context: CallbackContext):
    print('asd')
    chat_id = update.message.chat_id
    text = update.message.text
    first_name = update.message.chat['first_name']
    reply_text = f"Привет {first_name}! \n\n Вы зачем-то написали {text}"
    update.message.reply_text(text=reply_text)


class Command(BaseCommand):
    help = 'Телеграм бот'
    def handle(self, *args, **options):
        request = Request(
            connect_timeout=0.5,
            read_timeout=1.0
        )
        
        bot = Bot(
            request=request,
            token=settings.TELEGRAM_TOKEN,
        )
        print(bot.get_me())

        updater = Updater(
            bot=bot,
            use_context=True,
        )
        
        message_handler = MessageHandler(Filters.text, do_echo)
        updater.dispatcher.add_handler(message_handler)
        updater.start_polling()
        updater.idle()
